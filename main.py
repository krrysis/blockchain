"""
BLOCKCHAIN IMPLEMENTATION IN PYTHON
@AUTHOR: www.gitlab.com/krrysis

based on simple blockchain tut by siraj raval

blockchain is made of blocks
1st block is genesis block
rest other blocks have following data
1-index
2-data
3-ptr 2 next block
4-hash
5-nonce(number only used once)
6-previos hash
7-timestamp
"""
import datetime #for inputing dates
import hashlib #contains sha256

#we create a class for blocks here

class Block:
    blockIndex=0
    data=None
    ptr=None
    hash=None
    nonce=0
    prevHash=0x0
    timestamp=datetime.datetime.now()

    #we shall create an init function to define our block, it defines how we initialize our block

    def __init__(self, data):
        self.data=data

    # following is a function to compute hash of a block and it's attributees
    def hash(self):
        h=hashlib.sha256()
        h.update(
            str(self.nonce).encode('utf-8')+
            str(self.data).encode('utf-8')+
            str(self.prevHash).encode('utf-8')+
            str(self.timestamp).encode('utf-8')+
            str(self.blockIndex).encode('utf-8')
        )
        return h.hexdigest()

    #function to print a specific block
    def __str__(self):
        return "block hash: "+str(self.hash()) + "\n block index: "+str(self.blockIndex())+ "\n data: "+str(self.data())+"\previous hash"+str(self.prevHash())+"\n timestamp: "+str(self.timestamp())

#creating blockchain, blocks will be linked together

class Blockchain:
    maxnonce=2**32
    diff=10 #difficulty for mining
    target=2**(256-diff)
    #we shall define genesis block , the first block
    block=Block("this is the genesis block")
    #head is similar to top of stack
    head=block

    #function to add our block to the chain
    def add(self,block):
        block.prevHash=self.block.hash()
        block.blockIndex=self.block.blockIndex+1
        self.block.next=block
        self.blcok=self.block.next

    #proof of work, mining, whether or not a block can be added to chain
    def mine(self,block):
        for n in range(self.maxnonce):
            if int(block.hash(), 16) <= self.target:
                self.add(block)
                print(block)
                break
            else:
                block.nonce+=1

#initiallize blockchain
blockchain=Blockchain()

#loop to mine 10 block
for n in range(10):
    blockchain.mine(Block("Block"+str(n+1)))
    
#print blocks
while blockchain.head!=None:
    print(blockchain.head)
    blockchain.head=blockchain.head.next
    
